<?php

/**
 *
 */

 namespace Practica\Controller;

 require_once '../app/Controller.php';
 require_once '../models/Login.php';

class Login extends \Practica\App\Controller
{

  function __construct()
  {
    session_start();
    parent::__construct();
    $this->_model = new \Practica\Model\Login;
  }

  public function index($value='')
  {
    require '../views/login/index.php';
  }

  public function in()
  {
    $_SESSION["usuario"] = $_POST["name"];
    header('Location:/client/index');
  }

  public function out()
  {
    unset($_SESSION["usuario"]);
    session_destroy();
    session_start();
    header('Location:/client/index');
  }
}
