<?php

/**
 *
 */

 namespace Practica\Controller;

 require_once '../app/Controller.php';
 require_once '../models/Client.php';

class Client extends \Practica\App\Controller
{

  function __construct()
  {
    session_start();
    parent::__construct();
    $this->_model = new \Practica\Model\Client;
  }

  public function index($page=1)
  {
    $rows = $this->_model->get($page);
    $registros = $this->_model->numRegistros();
   
    require '../views/client/index.php';
  }
  public function seek(){
    $rows = $this->_model->seek($_POST["nameCliente"]);
    require '../views/client/seek.php';
  }
  public function add()
  {
    require '../views/client/new.php';
  }

  public function insert()
  {
    $this->_model->insert($_POST['name'],$_POST['dir'],$_POST['telf'],$_POST['credit']);
    header('Location: /client/index');
  }

  public function edit($id)
  {
    $row = $this->_model->getById($id);
    require '../views/client/edit.php';
  }

  public function update()
  {
    $this->_model->update($_POST['name'],$_POST['dir'],$_POST['telf'],$_POST['credit'],$_POST['id']);
    header('Location: /client/index');
  }

  public function delete($id)
  {
    $this->_model->delete($id);
    header('Location: /client/index');
  }

}
