<?php
require_once '../views/header.php';
?>

<div id="content">
  <?php
  if(!isset($_SESSION["usuario"])){
  ?>
    Invitado <a href="/login/index">Login</a>
    <br>
  <?php
  }else{
  ?>
    Te has registrado como <?=$_SESSION["usuario"];?>  <a href="/login/out">Salir </a>
    <br>
  <?php
  }
  ?>
  <h1>Modificar cliente</h1>
  <form  action="/client/update" method="post">
    <label for="name">Nombre:</label> 
    <input type="text" name="name" value="<?=$row["name"]?>">
    <br>
    <label for="dir">Dirección:</label>
    <input type="text" name="dir" value="<?=$row["dir"]?>">
    <br>
    <label for="telf">Teléfono:</label> 
    <input type="text" name="telf" value="<?=$row["telf"]?>">
    <br>
    <label for="credit">Creditos:</label> 
    <input type="text" name="credit" value="<?=$row["credit"]?>">
    <br>
    <input type="hidden" name="id" value="<?=$row["id"]?>">
    <input type="submit" name="" value="Modificar">
  </form>
</div>
<?php
require_once '../views/footer.php';
?>
