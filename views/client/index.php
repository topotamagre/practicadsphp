<?php
require_once '../views/header.php';
?>

<div id="content">
  <?php
  if(!isset($_SESSION["usuario"])){
  ?>
    Invitado <a href="/login/index">Login</a>
    <br>
  <?php
  }else{
  ?>
    Te has registrado como <?=$_SESSION["usuario"];?>  <a href="/login/out">Salir </a>
    <br>
  <?php
  }
  ?>
    <a href="/client/add">Nuevo Cliente</a>
    <br>
    <form action="/client/seek" method="post">
        Buscar Cliente
        <input type="text" name="nameCliente" value="" />
        <input type="submit" value="Enviar" />
    </form>
  <h2>Lista de Clientes</h2>

  <table>
    <tr>
      <th>ID</th>
      <th>Nombre</th>
      <th>Dirección</th>
      <th>Telefono</th>
      <th>Crédito</th>
      <th>Modificar</th>
      <th>Eliminar</th>
    </tr>
    <?php
          foreach ($rows as $array => $row) {
            ?>
            <tr>
              <td><?=$row['id']?></td>
              <td><?=$row['name']?></td>
              <td><?=$row['dir']?></td>
              <td><?=$row['telf']?></td>
              <td><?=$row['credit']?>€</td>
              <td><a href='/client/edit/<?=$row['id']?>'>Modificar </a></td>
              <td><a href='/client/delete/<?=$row['id']?>'>Eliminar</a></td>
            </tr>
        <?php
          }
        ?>
  </table>
  <p>Páginas:
      <?php
      $n = 0;
      while ($n < $many) {
      ?>
        <a href="/client/index/<?= $n+1 ?>"><?= $n+1 ?></a>
      <?php
        $n++;
      }
      ?>
    </p>
</div>
<?php
require_once '../views/footer.php';
?>
