<?php
require_once '../views/header.php';
?>
<div id="content">
  <?php
  if(!isset($_SESSION["usuario"])){
  ?>
    Invitado <a href="/login/index">Login</a>
    <br>
  <?php
  }else{
  ?>
    Te has registrado como <?=$_SESSION["usuario"];?>  <a href="/login/out">Salir </a>
    <br>
  <?php
  }
  ?>
  <h2>Añadir cliente</h2>
  <form  action="/client/insert" method="post">
    <label for="name">Nombre:</label> 
    <input type="text" name="name" value="<?=$row["name"]?>">
    <br>
    <label for="dir">Dirección:</label>
    <input type="text" name="dir" value="<?=$row["dir"]?>">
    <br>
    <label for="telf">Teléfono:</label> 
    <input type="text" name="telf" value="<?=$row["telf"]?>">
    <br>
    <label for="credit">Creditos:</label> 
    <input type="text" name="credit" value="<?=$row["credit"]?>">
    <br>
    <input type="submit" name="" value="Añadir">
  </form>
</div>
<?php
require_once '../views/footer.php';
?>
