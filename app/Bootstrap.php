<?php

/**
 *
 */

namespace Practica\App;

class Bootstrap
{

  public function __construct()
  {
    $url = $this->_getUrl();
    $this->_load($url);
  }

  private function _getUrl(){
      if (isset($_GET['url'])) {
          $url = rtrim($_GET['url'], "/");
          $url = explode("/", $url);
      } else {
          $url[0] = 'login';
          $url[1] = 'index';
      }
      if (count($url) == 1) {
         $url[1] = 'index';
      }
      return $url;
  }

  private function _load($url)
  {
    try {
      $controllerName = ucfirst(array_shift($url));
      if (file_exists('../controllers/'.$controllerName.".php")) {
        require_once '../controllers/'.$controllerName.".php";
        $controllerName = "\\Practica\\Controller\\".$controllerName;
        $controller = new $controllerName;
        $method = array_shift($url);
        
        if(method_exists($controller, $method)){
          call_user_func_array(array($controller,$method),$url);
        }else{
          throw new \Exception("No existe el método especificado", 1);
        }
      }else{
        throw new \Exception("Controlador $controllerName no encontrado", 1);
      }

    } catch (\Exception $e) {
      require_once '../controllers/Errorcontroller.php';
      $controller = new \Mvc\Controller\Errorcontroller;
      $controller->index($e->getMessage());
    }
  }
}
