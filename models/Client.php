<?php

/**
 *
 */

namespace Practica\Model;

class Client extends \Practica\App\Model
{
  const PAGE_SIZE = 10;

  function __construct()
  {
    parent::__construct();
  }

 public function get($page){
    
    $offset = ($page-1) * $this::PAGE_SIZE;
    $size = $this::PAGE_SIZE;
    $sql = "SELECT * FROM client LIMIT $size OFFSET $offset";
    $stmt = $this->_pdo->prepare($sql);
    $stmt->execute();
    $rows = $stmt->fetchAll(\PDO::FETCH_ASSOC);
    return $rows;
  }

  public function numRegistros(){
    $sql = "SELECT COUNT(id) AS c FROM client";
    $stmt = $this->_pdo->prepare($sql);
    $stmt->execute();
    $rows = $stmt->fetch(\PDO::FETCH_ASSOC);

    $registros = ceil($rows["c"] / $this::PAGE_SIZE);
    return $registros;
  }

  public function seek($name){
    $sql = "SELECT * FROM client WHERE name = :name";
    $stmt = $this->_pdo->prepare($sql);
    $stmt->bindValue(':name', $name);
    $stmt->execute();
    $rows = $stmt->fetchAll(\PDO::FETCH_ASSOC);
    return $rows;
  }
  public function getById($id){
    $sql = "SELECT * FROM client WHERE id = :id";
    $stmt = $this->_pdo->prepare($sql);
    $stmt->bindValue(':id', $id);
    $stmt->execute();
    $rows = $stmt->fetchAll(\PDO::FETCH_ASSOC);
    return $rows;
  }
  public function insert($name, $dir, $telf, $credit)
  {
    $sql = "INSERT into client (name, address, phone, credit) values (:name, :dir, :telf, :credit)";
    $stmt = $this->_pdo->prepare($sql);
    $stmt->bindParam('name',$name);
    $stmt->bindParam('dir',$dir);
    $stmt->bindParam('telf',$telf);
    $stmt->bindParam('credit',$credit);
    $stmt->execute();
  }

  public function update($name, $dir, $telf, $credit, $id)
  {
    $sql = "UPDATE client set name=:name, address=:dir, phone=:telf, credit=:credit where id=:id";
    $stmt = $this->_pdo->prepare($sql);
    $stmt->bindParam('name',$name);
    $stmt->bindParam('dir',$dir);
    $stmt->bindParam('telf',$telf);
    $stmt->bindParam('credit',$credit);
    $stmt->bindParam('id',$id);
    $stmt->execute();
  }

  public function delete($id)
  {
    $sql = "DELETE from client where id=:id";
    $stmt = $this->_pdo->prepare($sql);
    $stmt->bindParam('id',$id);
    $stmt->execute();
  }

}
